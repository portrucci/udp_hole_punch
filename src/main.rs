use std::{env, io::stdin, sync::mpsc, thread};
use sha2::{Digest, Sha224};
use rand::prelude::*;

mod peer;
use peer::Peer;

fn main() {
    let args: Vec<String> = env::args().collect();

    let (tx, rx) = mpsc::channel::<()>();
    thread::spawn(move || {
        loop {
            stdin().read_line(&mut String::new()).unwrap();
            tx.send(()).unwrap();
        }
    });

    let mut peer = Peer::connect_peer(args[1].as_str(), args[2].as_str(), args[3].as_str());

    loop {
        if let Ok(is_new) = peer.is_new_received() {
            if is_new || rx.try_recv().is_ok() {
                let (my_roll, my_key) = roll_dice();
                peer.send(my_roll.clone()).expect("Sending thread is somehow dead");

                let other_roll = peer.recv().expect("Listening thread is somehow dead");

                peer.send(my_key.clone()).expect("Sending thread is somehow dead");

                let other_key = peer.recv().expect("Listening thread is somehow dead");

                if is_peer_key_valid(other_key[..].try_into().unwrap(), other_roll[2..30].try_into().unwrap()) {
                    println!("{}  {}",
                             ((other_roll[0] ^ other_key[0]) + (my_roll[0] ^ my_key[0])) % 6 + 1,
                             ((other_roll[1] ^ other_key[1]) + (my_roll[1] ^ my_key[1])) % 6 + 1)
                } else {
                    // or maybe because udp is unreliable ¯\_(ツ)_/¯
                    panic!("peer is non-compliant")
                }
            }
        } else { panic!("Listening thread died") }
    }
}

/// Picks 2 random 8-bit numbers (1 for each die) and encrpyts them
/// using xor with a random 16-bit key.
///
/// returns a tuple which contains an array of 30 bytes
/// (2 for the encrypted roll, 28 for the sha224 digest of the key)
/// and the key used to encrypt the message, broken up into 2 bytes.
///
/// The hash of the key is also returned so the peer can verify that
/// the original key has not been changed after the initial encrypted
/// roll exchange. If the verification didn't happen a potential cheater
/// could change his key so the resulting decrypted roll will be in his favor.
fn roll_dice() -> (Vec<u8>, Vec<u8>) {
    let mut rng = thread_rng();
    let key: [u8; 2] = [rng.gen(), rng.gen()];
    let encrypted_roll: [u8; 2] = [rng.gen::<u8>() ^ key[0], rng.gen::<u8>() ^ key[1]];

    let mut hasher = Sha224::new();
    hasher.update(key);
    let digest = hasher.finalize();
    let mut encrypted_with_digest: Vec<u8> = Vec::with_capacity(30);
    encrypted_with_digest.extend(encrypted_roll.iter().chain(digest.iter()));

    (encrypted_with_digest, key.to_vec())
}

fn is_peer_key_valid(key: [u8; 2], digest: [u8; 28]) -> bool {
    let mut hasher = Sha224::new();
    hasher.update(key);
    let new_digest: [u8; 28] = hasher.finalize().try_into().unwrap();
    digest == new_digest
}
