use std::{io, net::UdpSocket, sync::mpsc::{self, Receiver, SendError, Sender}, thread};

pub struct Peer {
    tx: Sender<Vec<u8>>,
    rx: Receiver<Vec<u8>>,
    newly_received: Option<Vec<u8>>,
}

/// listens for data sent by the peer and sends it to another thread
fn handle_incoming(tx: Sender<Vec<u8>>, dest: &str, src: &str) -> io::Result<()> {
    let sock = UdpSocket::bind(src)?;
    sock.send_to(&[0], dest)?; // punches a udp hole
    loop {
        let mut buf = [0; 255];
        sock.recv(&mut buf)?;

        // receiver is dropped, should quit
        // probably not very smart
        if tx.send(buf.to_vec()).is_err() {
            return Ok(());
        } // TODO: make smarter
    }
}

/// listens for information sent by another thread
/// and then sends it to the peer
fn handle_outgoing(rx: Receiver<Vec<u8>>, dest: &str, src: &str) -> io::Result<()> {
    let sock = UdpSocket::bind(src)?;
    loop {
        let msg = match rx.recv() {
            Ok(m) => m,
            Err(_) => {
                // receiver is dropped, should quit
                // probably not very smart
                return Ok(());
            }
        };

        println!("sending: {:?}", msg);
        sock.send_to(&msg[..msg.len()], dest)?;
    }
}

impl Peer {
    pub fn connect_with_local_ports(
        addr: &str,
        src_port: &str,
        dest_port: &str,
        local_src_port: &str,
        local_dest_port: &str,
    ) -> Self {
        let local_send_addr = format!("0.0.0.0:{}", local_src_port);
        let local_listen_addr = format!("0.0.0.0:{}", local_dest_port);

        let src_addr = format!("{}:{}", addr, src_port);
        let dest_addr = format!("{}:{}", addr, dest_port);

        let (thread_tx, rx) = mpsc::channel::<Vec<u8>>();
        let (tx, thread_rx) = mpsc::channel::<Vec<u8>>();

        thread::spawn(move || {
            handle_incoming(thread_tx, src_addr.as_str(), local_listen_addr.as_str())
        });
        thread::spawn(move || {
            handle_outgoing(thread_rx, dest_addr.as_str(), local_send_addr.as_str())
        });

        Self {
            tx,
            rx,
            newly_received: None,
        }
    }

    /// connects to a peer with the same source and destination ports as the local peer
    pub fn connect_peer(addr: &str, src_port: &str, dest_port: &str) -> Self {
        Self::connect_with_local_ports(addr, src_port, dest_port, src_port, dest_port)
    }
}

impl Peer {
    pub fn send(&self, data: Vec<u8>) -> Result<(), SendError<Vec<u8>>> {
        self.tx.send(data)
    }

    pub fn recv(&mut self) -> Result<Vec<u8>, mpsc::RecvError> {
        if let Some(m) = self.newly_received.take() {
            Ok(m)
        } else {
            self.rx.recv()
        }
    }

    pub fn try_recv(&mut self) -> Result<Vec<u8>, mpsc::TryRecvError> {
        if let Some(m) = self.newly_received.take() {
            Ok(m)
        } else {
            self.rx.try_recv()
        }
    }

    pub fn is_new_received(&mut self) -> Result<bool, mpsc::TryRecvError> {
        if self.newly_received.is_some() {
            Ok(true)
        } else {
            match self.rx.try_recv() {
                Ok(m) => {
                    self.newly_received = Some(m);
                    Ok(true)
                },
                Err(mpsc::TryRecvError::Empty) => Ok(false),
                Err(mpsc::TryRecvError::Disconnected) => Err(mpsc::TryRecvError::Disconnected),
            }
        }
    }
}
